#ifndef SYSTEMOFEQUATIONS_H
#define SYSTEMOFEQUATIONS_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <unordered_set>
#include <string>
#include <iterator>
#include <iostream>
#include "Equation.h"

 class SystemOfEquations {
    private:
        std::map<std::string, Equation> _equationHashTable;
        int Solve(Equation &equation, std::unordered_set<std::string> &inProgress);

    public:
        SystemOfEquations(std::vector<Equation> equations);
        void Solve();
        void Print();
 };

#endif