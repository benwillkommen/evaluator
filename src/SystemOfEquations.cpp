#include "SystemOfEquations.h"

SystemOfEquations::SystemOfEquations(std::vector<Equation> equations){

    std::sort(equations.begin(), equations.end(),
        [] (Equation a, Equation b) { return a.get_definition() < b.get_definition(); });

    for(std::vector<std::string>::size_type i = 0; i != equations.size(); i++) {
        _equationHashTable.insert(std::pair<std::string, Equation>(equations[i].get_definition(), equations[i]));
    }
}

int SystemOfEquations::Solve(Equation &equation, std::unordered_set<std::string> &inProgress){
    // if equation is solved already, this loop will not execute
    for (auto const& variableName : equation.get_variables())
    {
        if(inProgress.find(variableName) == inProgress.end()){
            inProgress.insert(variableName);
            int variableValue = Solve(_equationHashTable.at(variableName), inProgress);
            equation.ReplaceVariable(variableName, variableValue);
        }        
    }

    if (equation.IsSolved()){
        inProgress.erase(equation.get_definition());
        return equation.get_accumulator();
    }

    throw "No solution found for variable: " + equation.get_definition();

}

void SystemOfEquations::Solve(){
    std::unordered_set<std::string> inProgress;
    for (auto &entry : _equationHashTable)
    {
        int solution = Solve(entry.second, inProgress);
    }
}

void SystemOfEquations::Print(){
    for (auto const& entry : _equationHashTable)
    {
        std::cout << entry.first << " =";
        std::unordered_set<std::string> variables = entry.second.get_variables();
        for (const auto& variable: variables) {
            std::cout << " " << variable;
        }
        std::cout << " " << entry.second.get_accumulator() << std::endl;
    }
}