#include "Equation.h"

//http://stackoverflow.com/a/37864920
bool Equation::hasOnlyDigits(const std::string s){
    return s.find_first_not_of( "0123456789" ) == std::string::npos;
}

Equation::Equation(std::string equationString){
    
    // as per the spec, this implementation assumes well formed input.
    // a complete implementation would need more robust input validation.
    std::vector<std::string> sides = Tokenizer::tokenize(equationString, '=');

    definition = Tokenizer::tokenize(sides[0], ' ')[0];

    std::vector<std::string> rightHandSide = Tokenizer::tokenize(sides[1], ' ');

    rightHandSide.erase(std::remove_if(
            rightHandSide.begin(),
            rightHandSide.end(), 
            [](const std::string s) -> bool { return s == "+"; }),
        rightHandSide.end());

    for(std::vector<std::string>::size_type i = 0; i != rightHandSide.size(); i++) {
        if(hasOnlyDigits(rightHandSide[i])){
            accumulator += std::stoi(rightHandSide[i]);
        } else {
            variables.insert(rightHandSide[i]);
        }
    }
}

int Equation::get_accumulator () const{
    return accumulator;
}
std::string Equation::get_definition() const{
    return definition;
}
std::unordered_set<std::string> Equation::get_variables() const{
    return variables;
}

bool Equation::IsSolved(){
    return variables.size() == 0;
}

bool Equation::ReplaceVariable(std::string variableName, int value){
    bool equationContainsVariable = variables.find(variableName) != variables.end();
    if(equationContainsVariable){
        accumulator += value;
        variables.erase(variableName);
        return true;
    }

    return false;
}