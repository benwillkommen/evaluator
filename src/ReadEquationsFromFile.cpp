#include "ReadEquationsFromFile.h"

std::vector<std::string> ReadEquationsFromFile(std::string fileName)
{
    std::vector<std::string> equations;

    std::ifstream file(fileName);
    if (file){
        std::string line;            
        while (std::getline(file, line))
        {            
            equations.push_back(line);            
        }
    }

    return equations;
}


