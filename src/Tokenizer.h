#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <algorithm>
#include <sstream>
#include <vector>
#include <iterator>

class Tokenizer { 
    public:
        static std::vector<std::string> tokenize(const std::string &s, char delim);

    private:
        //http://stackoverflow.com/a/236803
        template<typename Out>
        static void split(const std::string &s, char delim, Out result);
};

#endif