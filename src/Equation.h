#ifndef EQUATION_H
#define EQUATION_H

#include <algorithm>
#include <vector>
#include <unordered_set>
#include <iterator>
#include <iostream>
#include "Tokenizer.h"

class Equation {
    public:
        Equation(std::string equationString);
        int get_accumulator() const;
        std::string get_definition() const;
        std::unordered_set<std::string> get_variables() const;
        bool IsSolved();
        bool ReplaceVariable(std::string variableName, int value);


    private:
        bool hasOnlyDigits(const std::string s);
        std::string definition;
        std::unordered_set<std::string> variables;
        int accumulator = 0;
};

#endif