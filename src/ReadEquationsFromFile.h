#ifndef READEQUATIONSFROMFILE_H
#define READEQUATIONSFROMFILE_H

#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <iterator>

std::vector<std::string> ReadEquationsFromFile(std::string fileName);

#endif