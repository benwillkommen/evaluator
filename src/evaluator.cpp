#include "ReadEquationsFromFile.h"
#include "Equation.h"
#include "SystemOfEquations.h"
#include <iostream>
#include <vector>
#include <string>
#include <iterator>


int main(int argc,char *argv[]) {
    if (argc < 2)
    {
        std::cout << "usage: evaluator [filename]\r\n";
    }
    else 
    {
        std::string fileName = argv[1];
        std::vector<std::string> equationLines = ReadEquationsFromFile(fileName);
        if (equationLines.size() == 0)
        {
            std::cout << "No equations found in \"" << fileName << "\" not found.\r\n";
            return 0;
        }

        std::vector<Equation> equations;
        for(std::vector<std::string>::size_type i = 0; i != equationLines.size(); i++) {
            equations.push_back(Equation(equationLines[i]));
        }

        SystemOfEquations systemOfEquations(equations);
        systemOfEquations.Solve();
        systemOfEquations.Print();


    }
    
}