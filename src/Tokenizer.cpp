#include "Tokenizer.h"

std::vector<std::string> Tokenizer::tokenize(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));

    //http://stackoverflow.com/a/3385251
    //http://stackoverflow.com/a/9121837    
    elems.erase(std::remove_if(
            elems.begin(),
            elems.end(), 
            [](const std::string s) -> bool { return s.empty(); }),
        elems.end());

    return elems;
}

template<typename Out>
void Tokenizer::split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}