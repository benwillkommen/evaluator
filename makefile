default: all

all: test evaluator

test: ./test/tests.cpp Tokenizer.o Equation.o ReadEquationsFromFile.o
	g++ ./test/tests.cpp ./obj/Tokenizer.o ./obj/Equation.o ./obj/ReadEquationsFromFile.o -o run-tests

evaluator: evaluator.o
	g++ ./obj/evaluator.o ./obj/Tokenizer.o ./obj/ReadEquationsFromFile.o ./obj/Equation.o ./obj/SystemOfEquations.o -o evaluator

evaluator.o: ./src/evaluator.cpp ReadEquationsFromFile.o Tokenizer.o SystemOfEquations.o
	g++ -c ./src/evaluator.cpp -o ./obj/evaluator.o

Tokenizer.o: ./src/Tokenizer.cpp ./src/Tokenizer.h
	g++ -c ./src/Tokenizer.cpp -o ./obj/Tokenizer.o

ReadEquationsFromFile.o: ./src/ReadEquationsFromFile.cpp ./src/ReadEquationsFromFile.h
	g++ -c ./src/ReadEquationsFromFile.cpp -o ./obj/ReadEquationsFromFile.o

Equation.o: ./src/Equation.cpp ./src/Equation.h
	g++ -c ./src/Equation.cpp -o ./obj/Equation.o

SystemOfEquations.o: ./src/SystemOfEquations.cpp ./src/SystemOfEquations.h
	g++ -c ./src/SystemOfEquations.cpp -o ./obj/SystemOfEquations.o