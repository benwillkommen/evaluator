# Evaluator

This is a take home project for a tech interview I did.

## Building and Running
The default make target will produce the "evaluator" executable, which accepts an argument that is the path to the input file.

It will also produce an executable named "run-tests", which will run the project's unit tests using the Catch framework.

## Goals
Aside from actually making it work, my goals were to explore some of the newer features and idioms in C++, as I haven't touched C++ since college (7+ years ago). I recently heard a podcast with Kate Gregory on modern C++ and how there's less manual memory management new/delete fiddling necessary, and even lambdas now!

I also wanted to bring to the table some of the professionalism I've learned in the industry. Unit esting was not emphasized in my college c++ experience, so getting a test framework going was a must-have.

## The Process
To be honest, I did spend more than the 4 recommended hour on this, but a vast majority of that was setting up the initial project structure, remembering how and why header files work, and setting up the makefile to compile both my executable and my tests.

The problem itself was pretty clear from the start
1. Read in the each line of the file
2. Split it into tokens
3. Create a hashtable where the keys are the left hand side, values are the tokens of the right hand side
4. Loop through hashtable, examine right hand side tokens, recursively look up any variables in right hand side (be careful to avoid cycles)

Once I had all the C++ STL data structures and sytax figured out, it was easy.

The equation solving algorithm runs in O(N) time and uses O(N) space where N is the number of equations. However, the program as a whole is bounded by the call to stl::sort, which runs in O(n * log(n)) time.

## Things to Improve

### Handling Malformed/Bad Input
The spec said not to worry about it, but there are many scenarios that would require further consideration. e.g. what happens when an equation definition shows up more than one? Currently, an exception would be thrown when a duplicate key is inserted.

### Testing
Test case organization leaves much to be desired, and Catch even supports BDD style assertions. Alas, in the interest of time, I decided to just get _something_ working rather than falling down a test polishing rabbit hole.

I also did not test SystemOfEquations.Solve for this exercise in the interest of time.

### Makefile
My makefile is really repetitive, and is screaming "you're doing it wrong" at me. In its current form, it would not be maintainable as the project grows, and I'm sure there's a better way to do it, but I did not want to dump time into researching this for this exercise.

### Style
My indenting and whitespace are likely very inconsistent. I wrote this all in Sublime Text, and didn't bother configuring other than syntax highlighting. In practice, I would lean on IDEs/tooling to enforce consistent coding style.

Also, I know there is a way to avoid the ugly "std::" prefixes on everything, but didn't bother tracking down the fix.

## References
* http://stackoverflow.com/a/3385251
* http://stackoverflow.com/a/9121837
* http://stackoverflow.com/a/37864920
* http://stackoverflow.com/a/236803
