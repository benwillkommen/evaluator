#include "../src/Equation.h"
#include "../lib/catch.hpp"

TEST_CASE( "Equation constructor sets properties correctly", "[constructor]" ) {

    Equation eq = Equation("offset = 4 + random + 1");

    REQUIRE( eq.get_definition() == "offset" );
    REQUIRE( eq.get_variables().size() == 1 );
    REQUIRE( eq.get_variables().find("random") != eq.get_variables().end());
    REQUIRE( eq.get_accumulator() == 5 );
}

TEST_CASE( "IsSolved returns true when there are no more variables left", "[IsSolved]" ) {

    Equation eq = Equation("offset = 4 + 1");

    REQUIRE( eq.IsSolved() == true );
    REQUIRE( eq.get_accumulator() == 5 );
}

TEST_CASE( "IsSolved returns false when there are variables left", "[IsSolved]" ) {

    Equation eq = Equation("offset = 4 + 1 + asdf");

    REQUIRE( eq.IsSolved() == false );
    REQUIRE( eq.get_accumulator() == 5 );
}

TEST_CASE( "ReplaceVariable returns true if the variableName exists, removes it, and increments the accumulator", "[ReplaceVariable]" ) {

    Equation eq = Equation("offset = 4 + 1 + asdf");

    bool wasReplaced = eq.ReplaceVariable("asdf", 2);

    REQUIRE( wasReplaced == true );
    REQUIRE( eq.IsSolved() == true );
    REQUIRE( eq.get_accumulator() == 7 );
    REQUIRE( eq.get_variables().size() == 0 );
}

TEST_CASE( "ReplaceVariable returns false if the variableName does not exist", "[ReplaceVariable]" ) {

    Equation eq = Equation("offset = 4 + 1 + asdf");

    bool wasReplaced = eq.ReplaceVariable("notinthere", 2);

    REQUIRE( wasReplaced == false );
    REQUIRE( eq.get_accumulator() == 5 );
    REQUIRE( eq.get_variables().size() == 1 );
}
