#include "../src/ReadEquationsFromFile.h"
#include "../lib/catch.hpp"

TEST_CASE( "ReadEquationsFromFile returns the correct amount of lines", "[tokenize]" ) {
    
    std::vector<std::string> equations = ReadEquationsFromFile("./test/fixtures/equations.txt");

    REQUIRE( equations.size() == 4 );
}