#include "../src/Tokenizer.h"
#include "../lib/catch.hpp"

TEST_CASE( "Tokenizer.tokenize splits on spaces", "[tokenize]" ) {

    std::vector<std::string> tokens = Tokenizer::tokenize(" token1   token2 token3 ", ' ');

    REQUIRE( tokens.size() == 3 );
    REQUIRE( tokens[0] == "token1" );
    REQUIRE( tokens[1] == "token2" );
    REQUIRE( tokens[2] == "token3" );
}