

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../lib/catch.hpp"

#include "Tokenizer-tests.cpp"
#include "ReadEquationsFromFile-tests.cpp"
#include "Equation-tests.cpp"